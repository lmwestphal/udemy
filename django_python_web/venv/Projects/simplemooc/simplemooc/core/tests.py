from django.test import TestCase
from django.core import mail
from django.test.client import Client
from django.urls import reverse
# Create your tests here.

class ViewTest(TestCase):

    def test_home(self):
        client = Client()
        res = client.get(reverse('core:home'))
        self.assertEqual(res.status_code, 200)
    
    def test_home_template(self):
        client = Client()
        res = client.get(reverse('core:home'))
        self.assertTemplateUsed(res, 'home.html')
        self.assertTemplateUsed(res, 'base.html')


