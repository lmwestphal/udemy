from django.urls import path, re_path
from .views import index, thread_view, reply_correct, reply_incorrect

app_name = 'forum'
urlpatterns = [
    path('',index, name='home'),
    re_path(r'tag/(?P<tag>[\w_-]+)/$',index, name='home_tagged'),
    re_path(r'topico/(?P<slug>[\w_-]+)/$', thread_view, name='thread_detail'),
    re_path(r'reply/(?P<pk>\d+)/right$', reply_correct, name='reply_correct'),
    re_path(r'reply/(?P<pk>\d+)/wrong$', reply_incorrect, name='reply_incorrect')
]