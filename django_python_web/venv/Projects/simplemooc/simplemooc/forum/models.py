from django.db import models
from taggit.managers import TaggableManager
from django.conf import settings
from django.urls import reverse

# Create your models here.
class Thread(models.Model):

    title = models.CharField('Título', max_length=100)
    slug = models.SlugField('Slug', max_length=100, unique=True)
    body = models.TextField('Mensagem')
    author = models.ForeignKey(settings.AUTH_USER_MODEL,models.CASCADE,'threads_created', verbose_name='Autor')
    views = models.IntegerField('Visualizações',blank=True, default=0)
    answers = models.IntegerField('Respostas', blank=True, default=0)
    tags = TaggableManager()
    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    att_at = models.DateTimeField('Criado em', auto_now=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('forum:thread_detail', args=[self.slug])

    class Meta:
        verbose_name = 'Tópico'
        verbose_name_plural = 'Tópicos'
        ordering = ['-att_at']

class Reply(models.Model):
    thread = models.ForeignKey(Thread, models.CASCADE, 'thread_replies', verbose_name='Tópicos')
    answer = models.TextField('Resposta')
    author = models.ForeignKey(settings.AUTH_USER_MODEL,models.CASCADE,'replies_on_forums', verbose_name='Autor')
    right_ans = models.BooleanField('Resposta correta?', blank=True, default=False)
    created_at = models.DateTimeField('Criado em', auto_now_add=True) 
    att_at = models.DateTimeField('Criado em', auto_now=True)

    def __str__(self):
        return self.answer[:50]

    class Meta:
        verbose_name = 'Resposta'
        verbose_name_plural = 'Respostas'
        ordering = ['-right_ans','created_at']

def post_save_reply(created, instance, **kwargs):
    instance.thread.answers = instance.thread.thread_replies.count()
    instance.thread.save()
    if instance.right_ans:
        instance.thread.thread_replies.exclude(pk=instance.pk).update(right_ans=False)

def post_delete_reply(instance, **kwargs):
    instance.thread.answers = instance.thread.thread_replies.count()
    instance.thread.save()

models.signals.post_save.connect(post_save_reply, sender=Reply, dispatch_uid='post_save_reply')
models.signals.post_delete.connect(post_delete_reply, sender=Reply, dispatch_uid='post_delete_reply')