from django.contrib import admin
from .models import Thread, Reply
# Register your models here.

class ThreadAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug':['title']}

admin.site.register(Thread, ThreadAdmin)
admin.site.register(Reply)