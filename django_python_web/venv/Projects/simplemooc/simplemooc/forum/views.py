from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import TemplateView, ListView, DetailView, View
from .models import Thread, Reply
from .forms import ReplyForm
from django.contrib import messages
from django.http import HttpResponse
import json

# Create your views here.
class ForumView(ListView):
    context_object_name = 'threads'
    paginate_by = 10
    template_name = 'index_forum.html'

    def get_queryset(self):
        qs = Thread.objects.all()
        ord = self.request.GET.get('ord','')
        if ord == 'views':
            qs = qs.order_by('-views')
        elif ord == 'comments':
            qs = qs.order_by('-answers')

        tag = self.kwargs.get('tag','')
        if tag:
            qs = qs.filter(tags__slug__icontains=tag)
        return qs

    def get_context_data(self, **kwargs):
        ctx = super(ForumView, self).get_context_data(**kwargs)
        ctx['tags'] = Thread.tags.all()
        return ctx

class ThreadView(DetailView):
    model = Thread
    template_name = 'topic.html'
    context_object_name = 'thread'

    def get(self, request, *args, **kwargs):
        response = super(ThreadView, self).get(request, *args, **kwargs)
        if not self.request.user.is_authenticated or (self.object.author != self.request.user):
            self.object.views = self.object.views + 1
            self.object.save()
        return response

    def get_context_data(self, **kwargs):
        ctx = super(ThreadView, self).get_context_data(**kwargs)
        ctx['tags'] = Thread.tags.all()
        ctx['form'] = ReplyForm(self.request.POST or None)
        return ctx

    def post(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            messages.warning(self.request, 'Para responder ao tópico é necessário estar logado!')
            return redirect(self.request.path)
        self.object = self.get_object()
        ctx = self.get_context_data(object=self.object)
        f = ctx['form']
        if f.is_valid():
            reply = f.save(commit=False)
            reply.author = self.request.user
            reply.thread = self.object
            reply.save()
            messages.success(self.request, 'A sua resposta foi enviada com sucesso!')
            ctx['form'] = ReplyForm()
        return self.render_to_response(ctx)

class ReplyCorrectView(View):

    correct = True

    def get(self, request, pk):
        reply = get_object_or_404(Reply, pk=pk, thread__author=request.user)
        reply.right_ans = self.correct
        reply.save()
        msg = "Resposta atualizada com sucesso!"
        if request.is_ajax:
            data = {'success': True, 'message': msg}
            return HttpResponse(json.dumps(data), content_type='appication/json')
        else:
            messages.success(request, msg)
            return redirect(reply.thread.get_absolute_url())

index = ForumView.as_view()
thread_view = ThreadView.as_view()
reply_correct = ReplyCorrectView.as_view()
reply_incorrect = ReplyCorrectView.as_view(correct=False)