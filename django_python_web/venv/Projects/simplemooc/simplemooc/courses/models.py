from django.db import models
from django.urls import reverse
from django.conf import settings
from simplemooc.core.mail import send_mail_template
from django.utils import timezone

class CourseManager(models.Manager):

    def search(self, query):
        return self.get_queryset().filter(models.Q(name__icontains=query) | models.Q(slug__icontains=query))

# Create your models here.
class Course(models.Model):
    name = models.CharField('Nome', max_length=100)
    slug = models.SlugField('Atalho')
    desc = models.TextField('Descrição', blank=True)
    about = models.TextField('Sobre', blank=True)
    start_date = models.DateField('Data de início', null=True, blank=True)
    image = models.ImageField(upload_to='courses/images',verbose_name='Imagem', blank=True, null=True)
    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    att_at = models.DateTimeField('Criado em', auto_now=True)

    objects = CourseManager()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('courses:details', args=[self.slug])

    def get_released_lessions(self):
        return self.aulas.filter(release_date__gte=timezone.now().date())

    class Meta:
        verbose_name = 'Curso'
        verbose_name_plural = 'Cursos'
        ordering = ['name']

class Inscricao(models.Model):

    STATUS_CHOICES = (
        (0,'Pendente'),
        (1,'Aprovado'),
        (2,'Cancelado')
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Usuário', related_name='inscricoes', on_delete=models.CASCADE)
    course = models.ForeignKey(Course, verbose_name="Curso", related_name='cursos', on_delete=models.CASCADE)
    status = models.IntegerField('Situação', choices=STATUS_CHOICES, default=0, blank=True)
    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    att_at = models.DateTimeField('Atualizado em', auto_now=True)

    def __str__(self):
        return '{0} em {1}'.format(self.user, str(self.course))

    def activate(self):
        self.status = 1
        self.save()

    def is_aproved(self):
        return self.status == 1

    class Meta:
        verbose_name = 'Inscrição'
        verbose_name_plural = 'Inscrições'
        #unicidade: quase pk composta
        unique_together = (('user','course'))

class Anuncio(models.Model):
    course = models.ForeignKey(Course, verbose_name="Curso", related_name='anuncios', on_delete=models.CASCADE)
    title = models.CharField('Título', max_length=100)
    content = models.TextField('Conteúdo')
    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    att_at = models.DateTimeField('Atualizado em', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Anúncio'
        verbose_name_plural = 'Anúncios'
        ordering = ['-created_at']

class Comentario(models.Model):
    anuncio = models.ForeignKey(Anuncio, verbose_name='Anuncio', related_name='comentarios', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Usuário', related_name='user_comentarios', on_delete=models.CASCADE)
    coment = models.TextField('Comentário')
    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    att_at = models.DateTimeField('Atualizado em', auto_now=True)

    class Meta:
        verbose_name = 'Comentário'
        verbose_name_plural = 'Comentários'
        ordering = ['-created_at']

class Aula(models.Model):
    name = models.CharField('Nome',max_length=100)
    desc = models.TextField('Descrição', blank=True)
    sequence = models.IntegerField('Sequência', blank=True, default=0)
    release_date = models.DateField('Data de liberação', blank=True, null=True)
    course = models.ForeignKey(Course, verbose_name='Curso', related_name='aulas', on_delete=models.CASCADE)
    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    att_at = models.DateTimeField('Atualizado em', auto_now=True)

    def __str__(self):
        return self.name

    def is_avaliable(self):
        if self.release_date:
            return self.release_date <= timezone.now().date()
        return False

    class Meta:
        verbose_name = 'Aula'
        verbose_name_plural = 'Aulas'
        ordering = ['sequence']

class Material(models.Model):
    name = models.CharField('Nome',max_length=100)
    embedded = models.TextField('Material externo', blank=True)
    arq = models.FileField(upload_to='courses/aulas/material', blank=True, null=True)
    aula = models.ForeignKey(Aula, verbose_name='Aula', related_name='materiais', on_delete=models.CASCADE)

    def is_embeded(self):
        return bool(self.embedded)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Material'
        verbose_name_plural = 'Materiais'

def post_save_anuncio(instance, created, **kwargs):
    sub = instance.title

    ctx = {
        'anounce':instance
    }
    query = Inscricao.objects.filter(course=instance.course, status=1)
    for i in query:   
        send_mail_template(sub,'mail_anounce.html', ctx, [i.user.email])

models.signals.post_save.connect(post_save_anuncio, sender=Anuncio, dispatch_uid='post_save_anuncio')