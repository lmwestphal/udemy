from django.template import Library
from simplemooc.courses.models import Inscricao

register = Library()

@register.inclusion_tag("templatetags/my_courses.html")
def my_courses(user):
    ctx = {
        'insc': Inscricao.objects.filter(user=user)
    }
    return ctx

# adicionar no context o valor daqui
@register.simple_tag
def load_my_courses(user):
    return Inscricao.objects.filter(user=user)