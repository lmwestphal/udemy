from django.shortcuts import get_object_or_404, redirect
from django.contrib import messages
from .models import Course, Inscricao

def inscricao_required(view_func):
    def _wrapper(request, *args, **kwargs):
        slug = kwargs['slug']
        c = get_object_or_404(Course, slug=slug)
        has_permission = request.user.is_staff
        if not has_permission:
            try:
                insc = Inscricao.objects.get(user=request.user, course=c)
            except Inscricao.ObjectDoesNotExists:
                message = 'Você não tem permissão para acessar essa página'
            else:
                if insc.is_aproved():
                    has_permission = True
                else:
                    message = 'Inscrição pendente'
        if not has_permission:
            messages.error(request, message)
            return redirect('accounts:panel')
        request.course = c
        return view_func(request, *args, **kwargs)
    return _wrapper