from django.urls import path, re_path
from simplemooc.courses import views

app_name = 'courses'
urlpatterns = [
    path('',views.courses, name="home"),
    re_path(r'(?P<slug>[\w_-]+)/inscricao/$', views.inscrever, name='inscrever'),
    re_path(r'(?P<slug>[\w_-]+)/inscricao/desfazer$', views.undo_inscricao, name='undo_inscricao'),
    re_path(r'(?P<slug>[\w_-]+)/anuncios/$', views.anounce_course, name='anounce'),
    re_path(r'(?P<slug>[\w_-]+)/anuncios/(?P<anounce_pk>\d+)/$', views.show_anounce, name='show_anounce'),
    re_path(r'(?P<slug>[\w_-]+)/aulas/$', views.list_aulas, name='aulas'),
    re_path(r'(?P<slug>[\w_-]+)/aulas/(?P<aula_pk>\d+)/$', views.show_aula, name='show_aula'),
    re_path(r'(?P<slug>[\w_-]+)/material/(?P<material_pk>\d+)/$', views.material, name='material'),
    re_path(r'(?P<slug>[\w_-]+)/$', views.details, name='details')
]