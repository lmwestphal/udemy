from django.test import TestCase
from django.core import mail
from django.test.client import Client
from django.urls import reverse
from simplemooc.courses.models import Course
from django.conf import settings
from model_mommy import mommy

class CourseModelsTestCase(TestCase):

    def setUp(self):
        self.courses_django = mommy.make('courses.Course', name='python django', _quantity=10)
        self.courses_dev = mommy.make('courses.Course', name='python dev', _quantity=10)
        self.client = Client()
    
    def tearDown(self):
        Course.objects.all().delete()

    def test_course_search(self):
        self.assertEqual(len(Course.objects.search('django')), 10)
        self.assertEqual(len(Course.objects.search('dev')), 10)
        self.assertEqual(len(Course.objects.all()), 20)
        self.assertEqual(len(Course.objects.search('python')), 20)
