from django.test import TestCase
from django.core import mail
from django.test.client import Client
from django.urls import reverse
from simplemooc.courses.models import Course
from django.conf import settings
# Create your tests here.

class CourseFormsTestCase(TestCase):
    def setUp(self):
        self.course = Course.objects.create(name='Django', slug='django')

    def tearDown(self):
        self.course.delete()

    def test_contact_form_error(self):
        data = {'name':'Dunny', 'email':'', 'message':''}
        client = Client()
        path = reverse('courses:details', args=[self.course.slug])
        res = client.post(path, data)
        self.assertFormError(res, 'form', 'email', 'Este campo é obrigatório.')
        self.assertFormError(res, 'form', 'message', 'Este campo é obrigatório.')
    
    def test_contact_form_success(self):
        data = {'name':'Dunny', 'email':'test@teste.com', 'message':'mensagem nessa porra'}
        client = Client()
        path = reverse('courses:details', args=[self.course.slug])
        res = client.post(path, data)
        self.assertEqual(len(mail.outbox),1)
        self.assertEqual(mail.outbox[0].to,[settings.CONTACT_EMAIL])