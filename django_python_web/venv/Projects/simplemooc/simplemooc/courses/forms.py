from django import forms
from simplemooc.core.mail import send_mail_template
from django.conf import settings
from .models import Comentario

class ContactCourse(forms.Form):
    name = forms.CharField(label="Nome", max_length=100)
    email = forms.EmailField(label="Email")
    message = forms.CharField(label="Mensagem/Dúvida", widget=forms.Textarea)

    def send_mail(self, course):
        sub = '[%s] Contato' %course
        ctx = {
            'name':self.cleaned_data['name'],
            'email':self.cleaned_data['email'],
            'message':self.cleaned_data['message']
        }
        send_mail_template(sub,'mail_contato.html',ctx,[settings.CONTACT_EMAIL])

class ComentForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ['coment'] 

    