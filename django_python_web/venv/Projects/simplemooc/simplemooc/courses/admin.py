from django.contrib import admin
from .models import Course, Inscricao, Anuncio, Comentario, Aula, Material
# Register your models here.

class CourseAdmin(admin.ModelAdmin):
    list_display = ['name','slug','start_date']
    search_fields = ['name','slug']
    prepopulated_fields = {'slug':['name']}

class MaterialInlineAdmin(admin.StackedInline):
    model = Material

class AulaAdmin(admin.ModelAdmin):
    list_display = ['name','sequence','course', 'release_date']
    search_fields = ['name','desc']
    list_filter = ['created_at'] 
    inlines = [
        MaterialInlineAdmin
    ]

admin.site.register(Course, CourseAdmin)
admin.site.register([Inscricao, Anuncio, Comentario, Material])
admin.site.register(Aula, AulaAdmin)