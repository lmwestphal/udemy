from django.shortcuts import render, get_object_or_404, redirect
from .models import Course, Inscricao, Anuncio, Aula, Material
from .forms import ContactCourse, ComentForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .decorators import inscricao_required

# Create your views here.
def courses(request):
    courses = Course.objects.all()
    return render(request, 'index.html', {'courses':courses})

def details(request, slug):
    c = get_object_or_404(Course, slug=slug)
    context = {}
    form = ContactCourse(request.POST or None)
    if form.is_valid():
        context['is_valid'] = True
        form.send_mail(c)
        form = ContactCourse()

    context['course'] = c
    context['form'] = form
    return render(request, 'details.html', context)

@login_required
def inscrever(request, slug):
        c = get_object_or_404(Course, slug=slug)
        insc, created = Inscricao.objects.get_or_create(user=request.user, course=c)
        if created:
            insc.activate()
            messages.success(request,'Inscrito com sucesso no curso '+str(c))
        else:
            messages.info(request, 'você já está inscrito no curso '+str(c))
        return redirect('accounts:panel')

@login_required
def undo_inscricao(request, slug):
    c = get_object_or_404(Course, slug=slug)
    i = get_object_or_404(Inscricao, course=c, user=request.user)
    if request.method == "POST":
        i.delete()
        messages.success(request, 'Inscrição cancelada com sucesso')
        return redirect('accounts:panel')
    return render(request, 'inscricao_undo.html', {'course':i.course})

@login_required
@inscricao_required
def anounce_course(request, slug):
    return render(request, 'anounce.html', {'course':request.course})

@login_required
@inscricao_required
def show_anounce(request, slug, anounce_pk):
    c = request.course
    anounce = get_object_or_404(c.anuncios.all(), pk=anounce_pk)
    ctx = {}
    form = ComentForm(request.POST or None)
    if form.is_valid():
        comment = form.save(commit=False)
        comment.user = request.user
        comment.anuncio = anounce
        comment.save()
        messages.success(request, 'Comentário enviado com sucesso')
        form = ComentForm()
    ctx['course'] = c
    ctx['anuncio'] = anounce
    ctx['form'] = form
    return render(request, 'anounce_show.html', ctx)

@login_required
@inscricao_required
def list_aulas(request, slug):
    c = request.course
    less = c.get_released_lessions()
    if request.user.is_staff:
        less = c.aulas.all()
    return render(request, 'aulas.html', {'course':c,'aulas':less})

@login_required
@inscricao_required
def show_aula(request, slug, aula_pk):
    c = request.course
    aula = get_object_or_404(Aula, pk=aula_pk, course=c)
    if not request.user.is_staff or not aula.is_avaliable():
        messages.error(request,'Aula indisponível')
        return redirect('courses:aulas', slug=c.slug)

    return render(request, 'aula_show.html', {'course':c,'aula':aula})

@login_required
@inscricao_required
def material(request, slug, material_pk):
    c = request.course
    mat = get_object_or_404(Material, pk=material_pk, aula__course=c)
    if not request.user.is_staff or not mat.aula.is_avaliable():
        messages.error('Material indisponível')
        return redirect('courses:show_aula', slug=c.slug, aula_pk=mat.aula.pk)
    if not mat.is_embeded():
        redirect(mat.arq.url)
    return render(request, 'material.html', {'course':c,'aula':mat.aula,'material':mat})