from django.db import models
from django.conf import settings

# Create your models here.
class PasswordReset(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Usuário', on_delete=models.CASCADE, related_name='resets')
    key = models.CharField('Key', max_length=100, unique=True)
    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    confirmed = models.BooleanField('Confirmado', default=False, blank=True)

    def __str__(self):
        return '{0} em {1}'.format(self.user, self.created_at)

    class Meta:
        verbose_name = 'Reset de senha'
        verbose_name_plural = 'Resets de senha'
        ordering = ['-created_at']