from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from .models import PasswordReset
from simplemooc.core.utils import generate_hash_key
from simplemooc.core.mail import send_mail_template

User = get_user_model()

class CustomUserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Senha', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmação de senha', widget=forms.PasswordInput)

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('A confirmação de senha está incorreta')
        return password2

    def save(self, commit=True):
        user = super(CustomUserCreationForm, self).save(False)
        user.set_password(self.cleaned_data.get('password1'))
        if commit:
            user.save(True)

        return user

    class Meta:
        model = User
        fields = ['username', 'email']

class CustomEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'name']

class PasswordResetForm(forms.Form):
    email = forms.EmailField(label='Email')

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if User.objects.filter(email=email).exists():
            return email
        else:
            raise forms.ValidationError('Nenhum usuário encontrado com este email')

    def save(self):
        user = User.objects.get(email=self.cleaned_data.get('email'))
        res = PasswordReset(user=user, key=generate_hash_key(user.username))
        res.save()
        ctx = {'reset':res}
        send_mail_template('redefina sua senha no SimpleMOOC', 'password_reset_mail.html', ctx, [user.email])