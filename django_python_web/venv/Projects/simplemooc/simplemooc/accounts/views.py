from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import forms, authenticate, login, get_user_model
from django.conf import settings
from .forms import CustomUserCreationForm, CustomEditForm, PasswordResetForm
from django.contrib.auth.decorators import login_required
from .models import PasswordReset
from django.contrib import messages
from simplemooc.courses.models import Inscricao

User = get_user_model()

# Create your views here.
def cadastro(request):
    """ if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            user = authenticate(username=user.username, password=form.cleaned_data['password1'])
            login(request, user)
            return redirect('core:home')
    else:
        form = CustomUserCreationForm() """

    form = CustomUserCreationForm(request.POST or None) 
    if form.is_valid():
            user = form.save()
            user = authenticate(username=user.username, password=form.cleaned_data['password1'])
            login(request, user)
            return redirect('core:home')

    return render(request, 'new_user.html', {'form':form})

def reset_pass(request):
    ctx = {}
    form = PasswordResetForm(request.POST or None)
    if form.is_valid():
        form.save()
        ctx['success'] = True
    ctx['form'] = form
    return render(request, 'reset_pass.html', ctx)

def reset_confirm(request, key):
    ctx = {}
    reset = get_object_or_404(PasswordReset, key=key)
    form = forms.SetPasswordForm(user=reset.user, data=request.POST or None)
    if form.is_valid():
        form.save()
        ctx['success'] = True
    ctx['form'] = form
    return render(request, 'reset_confirm.html', ctx)

@login_required
def painel(request):
    ctx = {}
    return render(request, 'user_panel.html', ctx)

@login_required
def edit(request):
    ctx = {}
    """ if request.method == 'POST':
        form = CustomEditForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            form = CustomEditForm(instance=request.user)
            ctx['success'] = True
    else:
        form = CustomEditForm(instance=request.user) """

    form = CustomEditForm(request.POST or None, instance=request.user)
    if form.is_valid():
            form.save()
            messages.success(request,'Dados alterados com sucesso!')
            return redirect('accounts:panel')

    ctx['form'] = form
    return render(request, 'edit.html', ctx)

@login_required
def alter_senha(request):
    ctx = {}
    if request.method == "POST":
        form = forms.PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            ctx['success'] = True
    else:
        form = forms.PasswordChangeForm(user=request.user)

    ctx['form'] = form
    return render(request, 'edit-senha.html', ctx)