from django.urls import path
from django.contrib.auth import views as auth_views
from .views import cadastro, painel, edit, alter_senha, reset_pass, reset_confirm

app_name = 'accounts'
urlpatterns =[
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('cadastro/', cadastro, name="new-user"),
    path('logout/', auth_views.LogoutView.as_view(next_page='core:home'), name='logout'),
    path('painel/', painel, name="panel"),
    path('editar/', edit, name="edit"),
    path('password-change/', alter_senha, name="change-password"),
    path('password-reset/', reset_pass, name="reset"),
    path('reset-confirm/<key>/', reset_confirm, name="reset-confirm")
]