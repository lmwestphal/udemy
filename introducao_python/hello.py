# -*- coding: utf-8 -*-
import math

def maioridade():
	idade = input("Digite sua idade: ")
	try:
		idade = int(idade)
		if idade >= 18:
			print("Maior de idade")
		else:
			print("Menor de idade")
	except:
		print("idade digitada não foi um inteiro!")

def media():
	n1 = input("Digite a primeira nota: ")
	n2 = input("Digite a segunda nota: ")

	try:
		n1 = float(n1)
		n2 = float(n2)

		if (n1+n2)/2 >= 6:
			print("Aprovado")
		else:
			print("Reprovado")
	except:
		print("Alguma das notas informadas não é um valor válido!")

def equate():
	a = input("Digite o valor de A: ")
	b = input("Digite o valor de B: ")
	c = input("Digite o valor de C: ")

	try:
		a = float(a)
		b = float(b)
		c = float(c)

		delta = (b**2)-((4*a)*c)
		x1 = ((b*-1) + math.sqrt(delta))/(2*a)
		x2 = ((b*-1) - math.sqrt(delta))/(2*a)
		print("X1 = "+str(x1))
		print("X2 = "+str(x2))
	except:
		print("Algum dos valores informados não é um valor válido!")

def ord_list():
	lista = []
	try:
		for i in range(0,3):
			inp = input("Digite o "+str(i+1)+" valor: ")
			inp = int(inp)
			lista.append(inp)

		print("lista desordenada")
		print(lista)
		print("lista ordenada")
		lista.sort()
		print(lista)
	except:
		print("algum valor digitado não foi um inteiro!")

def calculate():
	v1 = input("Digite o primeiro valor: ")
	v2 = input("Digite o segundo valor: ")
	sin = input("Digite um sinal matemático: ")

	try:
		v1 = float(v1)
		v2 = float(v2)

		if sin = "+":
			print(str(v1)+" + "+str(v2)+" = "+(v1+v2))
		elif sin = "-":
			print(str(v1)+" - "+str(v2)+" = "+(v1-v2))
		elif sin = "*" or sin = "x":
			print(str(v1)+" * "+str(v2)+" = "+(v1*v2))
		elif sin = "/" or sin = "\\":
			if v2 = 0:
				print("Divisão por 0 não permitida")
			else:
				print(str(v1)+" / "+str(v2)+" = "+(v1/v2))
		elif sin = "^" or sin = "**":
			print(str(v1)+" ^ "+str(v2)+" = "+(v1**v2))
		elif sin = "%" or sin = "mod":
			print(str(v1)+" % "+str(v2)+" = "+(v1%v2))
		else:
			print("Sinal não definido!")
	except:
		print("Algum dos valores informados não é válido!")

if __name__ == '__main__':
	maioridade()
	print("----------------------")
	media()
	print("----------------------")
	equate()
	print("----------------------")
	ord_list()
	print("----------------------")
	calculate()
	print("----------------------")
	print("EOF")
